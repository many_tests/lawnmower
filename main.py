from utils import test_pattern


def main(inputs: list):
    if len(inputs) in range(0, 100):
        for i, case in enumerate(inputs):
            response = "YES" if test_pattern(**case) else "NO"
            print(f"Case #{i + 1}: {response}")
    else:
        print("Inputs cases can't be null or exceed 100")


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    from input import INPUTS

    main(INPUTS)
