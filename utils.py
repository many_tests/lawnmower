import functools


def test_pattern(n: int, m: int, pattern: list) -> bool:
    """
      If pattern is in 1 line/column is always possible
      else get max from all rows and columns (pre-calculate)
      and iterate on each square for test if less than max of its row and column
    """

    if n == 1 or m == 1:
        return True
    else:
        n_max = list(map(max, pattern))
        m_max = [functools.reduce(max, [pattern[i][j] for i in range(n)]) for j in range(m)]

        for i in range(0, n):
            for j in range(0, m):
                if pattern[i][j] < n_max[i] and pattern[i][j] < m_max[j]:
                    return False
        return True
