INPUTS = [{
    "n": 3,
    "m": 3,
    "pattern": [
        [2, 1, 2], 
        [1, 1, 1],
        [2, 1, 2]
    ]
}, {
    "n": 5,
    "m": 5,
    "pattern": [
        [2, 2, 2, 2, 2],
        [2, 1, 1, 1, 2],
        [2, 1, 2, 1, 2],
        [2, 1, 1, 1, 2],
        [2, 2, 2, 2, 2]
    ]
}, {
    "n": 1,
    "m": 3,
    "pattern": [
        [1, 2, 1]
    ]
}]
